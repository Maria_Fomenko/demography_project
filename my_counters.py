# -*- coding: utf-8
import pymorphy2
from nltk.tokenize import RegexpTokenizer


def tokenize(comments):
    tokenized_comments = []
    tokenizer = RegexpTokenizer('\w+|\$[\d\.]+|\S+')
    for comment in comments:
        tokenized_comment = tokenizer.tokenize(comment)
        tokenized_comments.append(tokenized_comment)
    return tokenized_comments


class LengthCounter:
    def fit_transform(comments, y=None):
        length_vector = []
        for comment in comments:
            len_list = []
            len_list.append(len(comment) * 0.3)
            length_vector.append(len_list)
        return length_vector

    def transform(comments):
        return LengthCounter.fit_transform(comments)


class CapsCounter:
    def fit_transform(comments, y=None):
        caps_vector = []
        tokenized_comments = tokenize(comments)
        for comment in tokenized_comments:
            caps_list = []
            sum = 0
            count = 0
            for word in comment:
                if word.isalpha():
                    sum += 1
                    if word.isupper():
                        count += 1
            if count == 0:
                caps_list.append(0)
            elif count < sum * 0.3:
                caps_list.append(1)
            elif count < sum * 0.7:
                caps_list.append(2)
            else:
                caps_list.append(3)
            caps_vector.append(caps_list)
        return caps_vector

    def transform(comments):
        return CapsCounter.fit_transform(comments)


class SexCounter:
    def fit_transform(comments, y=None):
        morph = pymorphy2.MorphAnalyzer()
        sex_vector = []
        tokenized_comments = tokenize(comments)
        for comment in tokenized_comments:
            sex_list = []
            male = 0
            female = 0
            for word in comment:
                parsed_word = morph.parse(word)[0]
                if 'VERB' in parsed_word.tag:
                    if 'femn' in parsed_word.tag:
                        female += 1
                    elif 'masc' in parsed_word.tag:
                        male += 1
            if female > male:
                sex_list.append(0)
            elif female == male:
                sex_list.append(1)
            elif male > female:
                sex_list.append(2)
            sex_vector.append(sex_list)
        return sex_vector

    def transform(comments):
        return SexCounter.fit_transform(comments)
