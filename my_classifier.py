# -*- coding: utf-8
from my_counters import LengthCounter
from my_counters import CapsCounter
from my_counters import SexCounter
from nltk.corpus import stopwords
import re
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.pipeline import FeatureUnion


class MyClassifier:
    def __init__(self):
        self.match_urls = re.compile(r'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+')
        self.stop_words = list(set(stopwords.words('russian')).union(set(stopwords.words('english'))))

        self.c_vector = CountVectorizer(max_df=0.8, ngram_range=(1, 3), stop_words=self.stop_words)
        self.estimators = [('cv', self.c_vector), ('sc', SexCounter), ('cc', CapsCounter), ('lc', LengthCounter)]
        self.combined = FeatureUnion(self.estimators)

        self.model = LogisticRegression()

    def clean(self, comment):
        comment = re.sub(self.match_urls, '', comment)
        comment = re.sub(r'(\<(/?[^>]+)>)', '', comment)
        comment = re.sub('^\w+\s*,\s*', '', comment)
        comment = re.sub(r'(\s|^)#\w+', '', comment)
        return comment

    def fit(self, comments, labels):
        for i in range(0, len(comments)):
            comments[i] = self.clean(comments[i])
        features = self.combined.fit_transform(comments)
        self.model.fit(features, labels)

    def predict(self, comments):
        for i in range(0, len(comments)):
            comments[i] = self.clean(comments[i])
        features = self.combined.transform(comments)
        return self.model.predict(features)

