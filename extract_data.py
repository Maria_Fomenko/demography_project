# -*- coding: utf-8
import json
import re
import requests


def extract():
    post_ids = []
    get_wall = 'http://api.vk.com/method/wall.get'
    get_comments = 'http://api.vk.com/method/wall.getComments'
    get_user = 'http://api.vk.com/method/users.get'

    res = requests.get(get_wall, {
        'owner_id': '-15755094',
        'count': '100'
    }).json()['response']

    for wall_object in res:
        if wall_object != res[0]:
            post_ids.append(wall_object['id'])

    female_comments = []
    male_comments = []
    match_urls = re.compile(r'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+')

    for id in post_ids:
        res = requests.get(get_comments, {
            'owner_id': '-15755094',
            'post_id': id,
            'count': '100'
        }).json()['response']
        for comment in res:
            if comment != res[0]:
                user = requests.get(get_user, {
                    'user_id': comment['from_id'],
                    'fields': 'sex'
                }).json()['response']
                if len(user) != 0:
                    text = comment['text']
                    text = re.sub(match_urls, '', text)
                    text = re.sub(r'(\<(/?[^>]+)>)', '', text)
                    if user[0]['sex'] == 1:
                        female_comments.append(text)
                    elif user[0]['sex'] == 2:
                        male_comments.append(text)
    return female_comments, male_comments


def save_data(female_comments, male_comments):
    data = []
    for comment in female_comments:
        element = {}
        element['text'] = comment
        element['sex'] = 0
        data.append(element)
    for comment in male_comments:
        element = {}
        element['text'] = comment
        element['sex'] = 1
        data.append(element)
    file_data = json.dumps(data)
    file = open('my_data.json', 'w')
    file.write(file_data)
    file.close()


if __name__ == "__main__":
    female_comments, male_comments = extract()
    save_data(female_comments, male_comments)
